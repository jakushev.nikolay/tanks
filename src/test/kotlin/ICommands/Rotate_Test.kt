package ICommands
import Commands.CommandsExeption
import IObject.IObjectInterface
import Rotate.Rotate
import org.junit.Test
import org.mockito.Mockito.*
import kotlin.test.assertFailsWith


class RotatorTest
{

    @Test
    fun RotateSetValue()
    {
        val mockIObject = mock(IObjectInterface::class.java)
        val mas = arrayOf(5.0)
        `when`(mockIObject.getValue("Rotate")).thenReturn(mas)
        `when`(mockIObject.getValue("Rotate Speed")).thenReturn(mas)
        val checkrotate=Rotate(mockIObject)
        checkrotate.Move()
        verify(mockIObject, atLeast(1)).setValue("Rotate",arrayOf(10.0))
    }

    @Test
    fun RotateSetArray()
    {
        val mockIObject:IObjectInterface
        mockIObject= mock(IObjectInterface::class.java)
        `when`(mockIObject.getValue("Rotate")).thenReturn(arrayOf(5.0,7.0))
        `when`(mockIObject.getValue("Rotate Speed")).thenReturn(arrayOf(5.0,7.0))
        val checkrotate=Rotate(mockIObject)
        checkrotate.Move()
        verify(mockIObject).setValue("Rotate",arrayOf(10.0,14.0))

    }

    @Test
    fun SizeExeption()
    {
        val mockIObject: IObjectInterface
        mockIObject = mock(IObjectInterface::class.java)
        `when`(mockIObject.getValue("Rotate")).thenReturn(arrayOf(5.0, 7.0))
        `when`(mockIObject.getValue("Rotate Speed")).thenReturn(arrayOf(5.0, 7.0, 9.0))
        val checkrotate = Rotate(mockIObject)
        assertFailsWith(CommandsExeption::class)
        {
            checkrotate.Move()
        }
    }


    @Test
    fun CommandCannotBeDone()
    {
        val mockIObject: IObjectInterface
        mockIObject = mock(IObjectInterface::class.java)
        `when`(mockIObject.getValue("Rotate")).thenReturn(arrayOf(5.0, 7.0))
        `when`(mockIObject.getValue("Rotate Speed")).then { throw IObject.ObjectGetException() }
        val checkrotate = Rotate(mockIObject)
        assertFailsWith(CommandsExeption::class)
        {
            checkrotate.Move()
        }
    }
}