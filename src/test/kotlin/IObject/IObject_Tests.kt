package IObject

import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.test.assertFailsWith

class IObject_Tests {

    @Test
    fun SetAndGetValue() {
        val testArray= mutableMapOf("test" to 9)
        val ob= IObject_Class()
        ob.setValue("Rotate Speed", 9)
        val getOb =ob.getValue("Rotate Speed", {})
        assertEquals(testArray["test"],getOb)
    }

    @Test
    fun SetAndGetArrayValue() {
        val testArray= mutableMapOf("test" to intArrayOf(9,10))
        var ob= IObject_Class()
        ob.setValue("Speed", intArrayOf(9,10))
        val getOb =ob.getValue("Speed", {})
        Assert.assertArrayEquals(testArray["test"],getOb as IntArray)
    }

    @Test
    fun LambdaMetodt() {
        var ob= IObject_Class()
        val getOb =ob.getValue("Key", {9})
        assertEquals(9, getOb)
    }

    @Test
    fun LambdaException() {
        var ob= IObject_Class()
        assertFailsWith(Exception::class) {
            ob.getValue("Key", {throw Exception()})}
    }
}