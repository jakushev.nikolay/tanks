package IObject

class IObject_Class:IObjectInterface
{
    private val IObject_map = hashMapOf<String, Any>()

    override fun getValue(_key:String, lambda:()->Any):Any
    {
        try
        {
            return  IObject_map.getOrDefault(_key, lambda())
        }catch(ex:Throwable)
        {
            throw Exception("invalid argument")
        }
    }
    override fun setValue(_key:String, newvalue:Any)
    {
        IObject_map.put(_key, newvalue)
    }
}