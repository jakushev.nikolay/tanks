package IObject
interface IObjectInterface
{
    fun getValue(_key:String, lambda:()->Any= {throw ObjectGetException()}):Any
    fun setValue(_key:String,value:Any)
}
class ObjectGetException:Error("no object values")