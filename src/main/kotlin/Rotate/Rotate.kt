package Rotate
import  IObject.IObjectInterface
import IObject.ObjectGetException
import Commands.CommandsExeption
class Rotate(private val obj:IObjectInterface)
{
    fun Move()
    {
        try
        {
            val rotate= obj.getValue("Rotate") as Array<Double>
            val speed = obj.getValue("Rotate Speed") as Array<Double>
            DiffValue(rotate,speed)

            for ((index,value) in rotate.withIndex())
            {
                rotate[index]= value+speed[index]
            }
            obj.setValue("Rotate", rotate)
        }

        catch(e: ObjectGetException)
        {
            throw CommandsExeption()
        }
        catch(e: ValueSizeException)
        {
            throw CommandsExeption()
        }
    }

    private fun DiffValue(rotate:Array<Double>, speed:Array<Double>)
    {
        if (rotate.size!==speed.size)
        {
            throw ValueSizeException("different value")
        }
    }
}

class ValueSizeException(s: String) : Throwable() {}

