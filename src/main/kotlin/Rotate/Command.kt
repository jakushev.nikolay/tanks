package Rotate

import Commands.ICommand

class Command(val Move: Rotate): ICommand
{
    override fun execute()
    {
        return Move.Move()
    }
}